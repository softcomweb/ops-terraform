variable "filename" {
    type = map(string)
    default = {
        "cat_path" = "./cat.txt"
        "dog_path" = "./dog.txt"
    }
}

variable "content" {
    type = map(string)
    default = {
        "msg1" = "I love CATS"
        "msg2" = "I love DOGS"
    }
}

variable "permission" {
    type = string
    default = "0777"
}
variable "pet_id" {
  type = string
  default = 11

  validation {
    condition = var.pet_id > 10
    error_message = "Sorry the Pet id cannot be less than 10"
  }
}

variable "pet_prefix" {
    type = string
    default = "Mr."
}

variable "pet_len" {
    type = number
    default = 1
}

# Example of an Object variable

variable "object_var" {
    type = object({
        name = string
        color = string
        age = number
        food = list(string)
        favorite_pet = bool
    })
    default = {
      age = 10
      color = "blue"
      favorite_pet = false
      food = [ "value1", "value2" ]
      name = "pet1"
    }
}