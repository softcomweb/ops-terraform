### Local Testing

Using http in vscode
https://marketplace.visualstudio.com/items?itemName=humao.rest-client

To make it easy to locally test Lambda functions packaged as container images we open-sourced a lightweight web-server, Lambda Runtime Interface Emulator (https://github.com/aws/aws-lambda-runtime-interface-emulator), which allows your function packaged as a container image to accept HTTP requests. You can install the AWS Lambda Runtime Interface Emulator on your local machine to test your function. Then when you run the image function, you set the entrypoint to be the emulator.

To install the emulator and test your Lambda function

- From your project directory, run the following command to download the RIE from GitHub and install it on your local machine.

```
mkdir -p ~/.aws-lambda-rie && \
    curl -Lo ~/.aws-lambda-rie/aws-lambda-rie https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie && \
    chmod +x ~/.aws-lambda-rie/aws-lambda-rie
```

- Run your Lambda image function using the docker run command.

```
docker run -d -v ~/.aws-lambda-rie:/aws-lambda -p 9000:8080 \
    --entrypoint /aws-lambda/aws-lambda-rie \
    myfunction:latest \
        /usr/local/bin/python -m awslambdaric app.handler
```

This runs the image as a container and starts up an endpoint locally at http://localhost:9000/2015-03-31/functions/function/invocations.

- Post an event to the following endpoint using a curl command:

```
curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'
```
