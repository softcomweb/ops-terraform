data "tls_certificate" "gitlab" {
  url = var.gitlab-url
}

resource "aws_iam_openid_connect_provider" "gitlab-oidc" {
  url             = var.gitlab-url
  client_id_list  = [var.gitlab-url]
  thumbprint_list = [data.tls_certificate.gitlab.certificates.0.sha1_fingerprint]
}

#Data object for pipeline assume role policy
data "aws_iam_policy_document" "pipeline_assume_role" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab-oidc.arn]
    }
    condition {
      test     = "StringLike"
      variable = "gitlab.com:sub"
      values   = var.gitlab-repositories
    }
  }
}

data "aws_iam_policy_document" "pipeline" {
  statement {
    actions = [
      "sts:GetCallerIdentity",
      "s3:*",
      "cloudformation:*",
      "dynamodb:*",
      "lambda:*",
      "ecr:*",
      "ecs:*",
      "cloudwatch:*",
      "logs:*",
      "cloudfront:*",
      "events:*"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "pipeline_role" {
  name                = var.role_name
  assume_role_policy  = data.aws_iam_policy_document.pipeline_assume_role.json
  managed_policy_arns = [aws_iam_policy.pipeline_policy.arn]
}
#CICD pipeline IAM policy
resource "aws_iam_policy" "pipeline_policy" {
  name   = "gitlab-aws-openid-connect-policy"
  policy = data.aws_iam_policy_document.pipeline.json
}
#Attach policy to role
resource "aws_iam_role_policy_attachment" "pipeline" {
  role       = aws_iam_role.pipeline_role.name
  policy_arn = aws_iam_policy.pipeline_policy.arn
}

output "assume_role_arn" {
  value       = aws_iam_role.pipeline_role.arn
  description = "Gitlab oidc iam role arn"
}

output "assume_role_name" {
  value       = aws_iam_role.pipeline_role.name
  description = "Gitlab oidc iam role name"
}