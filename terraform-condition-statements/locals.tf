locals {
    # All json data
    all_json_data = jsondecode(file("config.json"))

    # Load first list
    list1_data = jsondecode(file("config.json")).list1

    # Load first map
    map1_data = local.all_json_data.map1

    # second map
    map2_data = local.all_json_data.map2

}