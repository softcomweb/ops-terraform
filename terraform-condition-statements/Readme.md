# Run terraform init and then you can go to your terminal and run terraform console

# show all data

> local.all_json_data

# show all map1

> local.map1_data

# list iteration

> [ for i in local.all_json_data.list1 : i ]

# list with condition statement

> [ for i in local.all_json_data.list1 : i if i != "list1"]

# iterate through map

> { for k,v in local.map1_data : v => k }

# map with condition

> { for k,v in local.map1_data : v => k if k != "mapKey1" }

# loop over list of dic with condition
> [ for i in local.all_json_data.list2 : i if i.foo == "bar" ]


### For Syntax

[ for list_items in local.list_data : data_in_list ]