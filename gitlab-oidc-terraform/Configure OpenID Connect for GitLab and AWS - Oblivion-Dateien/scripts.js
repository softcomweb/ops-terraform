/**
 * This script must be loaded as type="module" (see functions.php).
 * Modules are automagically loaded asynchronously
 * and executed in source order when the DOM is ready.
 * Works in all modern browsers (i.e. not Internet Explorer 11).
 */

const NAV_ANIMATION = 200;

function toggle_desktop_menu(event_object) {
    jQuery(this).toggleClass('is-open');
    event_object.preventDefault();
}

jQuery('.has-menu').click(toggle_desktop_menu);

function toggle_mobile_menu(e) {
    jQuery('body').toggleClass('nav-open');
    e.preventDefault();
}

jQuery('.btn-nav').on('click', toggle_mobile_menu);

function toggle_mobile_navigation_dropdown() {
    const $this = jQuery(this);

    if ($this.hasClass('is-open')) {
        $this.removeClass('is-open').find('.sub-menu').slideUp(NAV_ANIMATION);
    } else {
        $this.addClass('is-open').find('.sub-menu').slideDown(NAV_ANIMATION);
    }
}

jQuery('.nav-mobile .menu-item-has-children').click(
    toggle_mobile_navigation_dropdown
);

function toggle_targets() {
    const $this = jQuery(this);

    if ($this.hasClass('is-open')) {
        $this
            .removeClass('is-open')
            .closest('.toggle-box')
            .find('.toggle-target')
            .slideUp(NAV_ANIMATION);
    } else {
        $this
            .addClass('is-open')
            .closest('.toggle-box')
            .find('.toggle-target')
            .slideDown(NAV_ANIMATION);
    }
}

jQuery('.toggle-title').click(toggle_targets);

//Scroll to hashtag
function scroll_to_hash_tag() {
    if (
        location.pathname.replace(/^\//, '') ==
            this.pathname.replace(/^\//, '') &&
        location.hostname == this.hostname
    ) {
        const DURATION = 600;
        let target = jQuery(this.hash);

        target = target.length
            ? target
            : jQuery('[name=' + this.hash.slice(1) + ']');

        if (target.length) {
            jQuery('html,body').animate(
                {
                    scrollTop: target.offset().top,
                },
                DURATION
            );

            return false;
        }
    }
}

jQuery('a[href*="#"]:not([href="#"]):not([href^="#="])').click(
    scroll_to_hash_tag
);

//Sticky nav
jQuery(window).on('scroll', function () {
    var pos = jQuery(this).scrollTop();
    if (pos >= 50) {
        jQuery('.nav-top').addClass('is-gone');
    } else {
        jQuery('.nav-top').removeClass('is-gone');
    }
});

//Sliders
var $casesSlider = jQuery('.cases-slider-main').flickity({
    pageDots: false,
    initialIndex: 1,
    bgLazyLoad: 2,
    fade: true,
    wrapAround: true,
    // adaptiveHeight: true,
    prevNextButtons: false,
});

jQuery('.cases-nav .btn-left').on('click', function () {
    $casesSlider.flickity('previous');
});
jQuery('.cases-nav .btn-right').on('click', function () {
    $casesSlider.flickity('next');
});

var $casesLogosSlider = jQuery('.cases-slider-nav').flickity({
    asNavFor: '.cases-slider-main',
    contain: true,
    pageDots: false,
    prevNextButtons: false,
    imagesLoaded: true,
    initialIndex: 1,
});

jQuery('.logos-nav .btn-left').on('click', function () {
    $casesSlider.flickity('previous');
});
jQuery('.logos-nav .btn-right').on('click', function () {
    $casesSlider.flickity('next');
});

//Small slider
var $casesSmSlider = jQuery('.slider-cases-sm').flickity({
    pageDots: true,
    wrapAround: false,
    initialIndex: 0,
    prevNextButtons: false,
    bgLazyLoad: 2,
    cellAlign: 'left',
});

//Team Slider
jQuery('.team-slider-wrap').each(function () {
    var $teamSlider = jQuery(this).find('.team-slider').flickity({
        pageDots: false,
        prevNextButtons: false,
        cellSelector: '.team-slider-box',
        imagesLoaded: true,
        groupCells: 1,
        cellAlign: 'left',
        selectedAttraction: 0.01,
        friction: 0.15,
    });
    var prevBtn = jQuery(this).find('.btn-left');
    var nextBtn = jQuery(this).find('.btn-right');
    jQuery(prevBtn).on('click', function () {
        $teamSlider.flickity('previous');
    });
    jQuery(nextBtn).on('click', function () {
        $teamSlider.flickity('next');
    });
});

//Logos Slider
jQuery('.logo-slider-wrap').each(function () {
    var $teamSlider = jQuery(this).find('.logo-slider').flickity({
        contain: true,
        pageDots: false,
        prevNextButtons: false,
        imagesLoaded: true,
        cellAlign: 'left',
        // groupCells: true,
        wrapAround: true,
        initialIndex: 1,
    });
    var prevBtn = jQuery(this).find('.btn-left');
    var nextBtn = jQuery(this).find('.btn-right');
    jQuery(prevBtn).on('click', function () {
        $teamSlider.flickity('previous');
    });
    jQuery(nextBtn).on('click', function () {
        $teamSlider.flickity('next');
    });
});

//Service box slider
jQuery('.boxes-slider-group').each(function () {
    var $boxesSlider = jQuery(this).find('.boxes-slider-main').flickity({
        contain: true,
        cellSelector: '.service-slide-wrap',
        pageDots: true,
        cellAlign: 'left',
        prevNextButtons: false,
        wrapAround: false,
    });
});

var $casesNavSlider = jQuery('.boxes-slider-nav').flickity({
    asNavFor: '.boxes-slider-main',
    contain: true,
    pageDots: false,
    prevNextButtons: false,
    imagesLoaded: true,
    cellAlign: 'left',
});

//Staggered delays
function addDelay(delayTarget, motionType, delayCounter, delayAmount) {
    jQuery(delayTarget).each(function () {
        jQuery(this)
            .children()
            .each(function () {
                delayCounter += delayAmount;
                jQuery(this).css(motionType + '-delay', delayCounter + 's');
            });
    });
}
// addDelay('.data-appear-group', 'transition', -0.20, 0.20);
addDelay('.data-appear-group-sm', 'transition', 0.2, 0.1);
addDelay('.h-delay', 'transition', 0.4, 0.1);

//Staggered delays
function addDelayOld(delayTarget, delayAmount) {
    jQuery(delayTarget).each(function () {
        var delayCounter = -0.15;
        jQuery(this)
            .children()
            .each(function () {
                delayCounter += delayAmount;
                jQuery(this).css('transition-delay', delayCounter + 's');
            });
    });
}
addDelayOld('.data-appear-group', 0.15);
