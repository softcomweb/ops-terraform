terraform {
  required_providers {
    keycloak = {
      source  = "mrparkers/keycloak"
      version = ">= 4.0.0"
    }
  }
}

provider "keycloak" {
  client_id     = "fkt-client"
  client_secret = "zmNkzM5MC0gWLDjfWp0rUNoLx86PhIpB"
  url           = "http://localhost:8080"
  initial_login = false
  realm = "softcomweb"
}
