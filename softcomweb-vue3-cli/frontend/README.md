## Vue3.js

Vue is a Framework for creating Dynamic, Event-Driven and interactive website

- It uses the new Composition api with the setup() function
- With Vue3, you can also create multiple root elements side-by-side in a component
```
<template>
    <div>
        <p> Hallo from First Element </p>
    </div>
    <div>
        <p> Hallo from Second Element </p>
    </div>
</template>
```
- Use of the Teleport Component
    * This allows you to render content from one component in a     different place in the DOM
    * Useful for things like Modals

- Use of the Suspense Component
    * Used to handle Async components easily
    * can provide fall-back contents ( eg a spinner ) until data is loaded

- Supports Typescript out of the box so one can now write Vue app using typescript