const app = Vue.createApp({
    //template: '<h2>I am a content from the Vue app</h2>'
    data() {
        return {
            title: "Afro man in Germany",
            firstname: "leacoco",
            lastname: "kolo",
            city: {
                name: "Hamburg",
                code: 22305
            },
            people: [
                {
                    firstname: "leacoco",
                    lastname: "kolo",
                    isParent: true,
                    amount: 10
                },
                {
                    firstname: "nic",
                    lastname: "kolo",
                    isParent: true,
                    amount: 20
                },
                {
                    firstname: "jam",
                    lastname: "kolo",
                    isParent: false,
                    amount: 30
                }
            ],
            num: 0,
            showCity: true,
            showfirstname: true,
            url: 'http://localhost:8080'
        }
    },
    methods: {
        increaseNumber() {
            this.num++
            if(this.num == 10 || this.num == 20) {
                this.showCity = !this.showCity
            }else {
                this.showCity = true
            }
        },
        showFirstName() {
            this.showfirstname = !this.showfirstname
        },
        togglePerson(person) {
            person.isParent = !person.isParent
        }
    },
    computed: {
        filtered_parents() {
            return this.people.filter(p => p.isParent == true)
        }
    }
})

app.mount('#app')