resource "local_file" "pet" {
    filename = var.filename["dog_path"]
    content = var.content["msg2"]
    file_permission = var.permission
}

resource "local_file" "pet2" {
    filename = var.filename["cat_path"]
    content = "My cat name is ${random_pet.pet2.id}"

    depends_on = [
      random_pet.pet2
    ]
}

resource "random_pet" "pet2" {
    keepers = {
        pet_id = var.pet_id
    }
    length = var.pet_len
    prefix = var.pet_prefix
    separator = ""
}

output "file_absolute_path" {
    value = "${path.cwd}/${local_file.pet.filename}"
    description = "The absolue path of the file"
}