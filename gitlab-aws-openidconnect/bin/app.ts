#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { GitlabAwsOpenidconnectStack } from '../lib/gitlab-aws-openidconnect';


const app = new cdk.App({
  context: {},
  outdir: process.env.CDK_OUT_DIR || "cdk.out"

});

const appName = "gitlab-oidc";
const appEnvironment = app.node.tryGetContext("@softcomweb-ec:core:appEnvironment") || "infra";
const awsAccount = app.node.tryGetContext("@softcomweb-ec:core:awsAccount") || process.env.CDK_DEFAULT_ACCOUNT;
const awsRegion = app.node.tryGetContext("@softcomweb-ec:core:awsRegion") || process.env.CDK_DEFAULT_REGION;


const appEnv = {
  region: awsRegion,
  account: awsAccount,
}

new GitlabAwsOpenidconnectStack(app, 'GitlabAwsOpenidconnectStack', {
  stackName: appName,
  env: appEnv,
});

cdk.Tags.of(app).add("team", "ops");
cdk.Tags.of(app).add("vertical", "gitlabOidc");
cdk.Tags.of(app).add("environment", appEnvironment);
cdk.Tags.of(app).add("service", appName);

if (module.children) {
  app.synth();
}