data "keycloak_realm" "realm" {
  realm = "softcomweb"
}

resource "keycloak_group" "admin_group" {
  realm_id = data.keycloak_realm.realm.id
  name     = "admin-group"
}

resource "keycloak_group" "dev_group" {
  realm_id  = data.keycloak_realm.realm.id
  name      = "dev-group"
}