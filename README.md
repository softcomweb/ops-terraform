## Getting started with Terraform

########### Options of installing Localstack on your machine
Using the python pip

-----
python -m pip install localstack
Starting localstack for development

>> localstack --profile=dev start -d

and the dev.env variable file is located at: ~/.localstack/dev.env

-----
Using docker run

> docker run --rm -it -p 4566:4566 -p 4571:4571 localstack/localstack


# start localstack

> localstack start -d

> localstack status

> localstack config show

## Clone terraform project directory to get started

sh -c "$(curl -fsSL https://gist.githubusercontent.com/leacoco/2833d084e583bb532e7438f0e12e8fc5/raw/c4a45ee23ac118f2ccfc317011b26b69204bd59b/terraform-project-init.sh)"

IAC (Infrastructure as A Code tool) infrastructure provisioning tool

- Kubernetes is Declerative ( Desire state for your infrastructure )
- Terraform takes care of moving from Desired state to Require state

Terraform init => initializes the Project and identifies Providers needed

Terraform plan => Plan how to get there

Terraform apply => makes changes to target

# Gitlab-Aws OIDC configuration
https://gitlab.com/renjithvr11/gitlab-aws-openidconnect

# Resource

Every object that terraform manages is called a RESOURCE

- file on localhost
- ec2 instances
- db
- iam users etc etc
  Terraform records the state for all its resources in a terraform.tfstate file and determins what action to take depending on the state

create -> update -> delete

# HCL syntax

The HCL Syntax consist of blocks and arguments

<block> <parameters> {
key1 = value1
key2 = value2
}

example:
resource "local_file" "pet" {
filename = "/tmp/test.txt"
content = "Hallo World"
}

- The block name = "resource"
- The resource type = "local_file" # resource type (a fix value depending on the Provider for the resource type) where by
  - local = Provider
  - file = Provider resource
- The resource name = "pet" # the logical name to identify the resource and it can be name anything.

- within the block {} we define the arguments specific to the Provider resource type

# Variable types

- string
- number
- bool
- any ( default value when no type is set)
- list ["cat", "dog"]
- map pet1=cat
      pet2=dog
- object complex Data Structure
- tuple complet Data Structure

* maps cannot contain duplicates
* tuples are like list but can be of different types

variable "cat" {
    type    = tuple([string, number, bool])
    default = ["cat", 3, true]
}

Passing variables to Terraform
- If you have a variable defined with no default value, you can pass in the variable during terraform apply in a non interractive way

> terraform apply -var "variable1_name=value" -var "variable2_name=value" -var "variable3_name=value" 

or you can use environment variables
export TF_VAR_variable1_name="value"
export TF_VAR_variable2_name="value"
export TF_VAR_variable3_name="value"

or you can also use a variable file and pass that via the command line

terraform apply -var-file <PATH_TO_VAR_FILE>
NOTE: the variable file must end with a .tfvars or .tfvar.json ending and must be named
terraform.tfvars
terraform.tfvars.json
*.auto.tfvars
*.auto.tfvars.json

and will be automatic loaded by terraform

any other name should be passed in on the command line with -var-file <file_name>.tfvars

example:

--- terraform.tfvars
variable1_name = "value"
variable2_name = "value"
variable3_name = "value"

> terraform apply 

How to use lambda layer with cdk: https://www.ranthebuilder.cloud/post/build-aws-lambda-layers-with-aws-cdk

## Image with aws and terraform installed
>> zenika/terraform-aws-cli