!(function () {
  if (!window.EncTracking || !window.EncTracking.started) {
    window.EncTracking = {
      queue:
        window.EncTracking && window.EncTracking.queue
          ? window.EncTracking.queue
          : [],
      track(t) {
        this.queue.push({ type: "track", props: t });
      },
      identify(t) {
        this.queue.push({ type: "identify", props: t });
      },
      started: !0,
    };
    const t = window.EncTracking;
    (t.writeKey = "yk6sG898KcUpvV3f5n6ht8fTA"),
      (t.hasOptedIn = true),
      (t.shouldGetConsent = true),
      t.hasOptedIn && (t.shouldGetConsent = !1),
      (t.optIn = function () {
        (t.hasOptedIn = !0), t && t.init();
      }),
      (t.optOut = function () {
        (t.hasOptedIn = !1), t && t.setOptOut && t.setOptOut(!0);
      });
    const n = function (t) {
      const n = document.createElement("script");
      (n.type = "text/javascript"),
        (n.async = void 0 === t || t),
        (n.src = "https://resources-app.encharge.io/encharge-tracking.min.js");
      const e = document.getElementsByTagName("script")[0];
      e.parentNode.insertBefore(n, e);
    };
    document.readyState === "complete"
      ? n()
      : window.attachEvent
      ? window.attachEvent("onload", n)
      : window.addEventListener("load", n, !1);
  }
})();
