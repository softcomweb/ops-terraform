variable "bucket_name" {
    description = "S3 Bucket for storing the Terraform state file"
    type = string
    validation {
        condition = can(regex("^([a-z0-9]{1}[a-z0-9-]{1,61}[a-z0-9]{1})$", var.bucket_name))
        error_message = "Bucket Name must not be empty and must follow S3 naming rules."
    }
}