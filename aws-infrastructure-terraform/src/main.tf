terraform {

  backend "s3" {
    bucket  = "softcomweb-terraform-state-backend"
    key     = "terraform-infra/terraform.tfstate"
    region  = "eu-central-1"
    encrypt = true
  }

  required_version = ">= 0.13.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
  default_tags {
    tags = {
      environment = "infra"
      name        = "terraform-infra"
      service     = "foundation"
      vertical    = "aws"
    }
  }
}

module "tf-state" {
  source      = "./modules/tf-state"
  bucket_name = "softcomweb-terraform-state-backend"
}

module "tf-state2" {
  source      = "./modules/tf-state"
  bucket_name = "softcomweb-terraform-state-backend2"
}
