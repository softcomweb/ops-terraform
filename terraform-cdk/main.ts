import { Construct } from "constructs";
import { App, TerraformStack } from "cdktf"
import { DockerProvider } from "@cdktf/provider-docker/lib/provider";
//import { AwsProvider } from "@cdktf/provider-aws/lib/provider";
import { Image } from "@cdktf/provider-docker/lib/image";
import { Container } from "@cdktf/provider-docker/lib/container";

class MyStack extends TerraformStack {
  constructor(scope: Construct, name: string) {
    super(scope, name)

    new DockerProvider(this, "docker", {});

    const dockerImage = new Image(this, "nginxImage", {
      name: "nginx:latest",
      keepLocally: false
    });

    new Container(this, "nginxContainer", {
      name: "softcomweb-container",
      image: dockerImage.name,
      ports: [
        {
          internal: 80,
          external: 9090,
        },
      ],
    });
  }
}

const app = new App()
new MyStack(app, "softcomweb-cdk-docker")
app.synth()