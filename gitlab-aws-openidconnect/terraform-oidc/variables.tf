variable "gitlab-url" {
  type    = string
  default = "https://gitlab.com"
}

variable "gitlab-repositories" {
  type = list(any)
  default = [
    "project_path:softcomweb/ops-terraform:ref_type:branch:ref:main"
  ]
}


variable "role_name" {
  type    = string
  default = "gitlab-terraform-oidc-role"
}