import * as cdk from 'aws-cdk-lib';
import { OpenIdConnectProvider, Policy, Conditions, Role, WebIdentityPrincipal, PolicyDocument, PolicyStatement, ManagedPolicy } from 'aws-cdk-lib/aws-iam';
import { Construct } from 'constructs';
import { repositoriesConfig } from './repositories';


export class GitlabAwsOpenidconnectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps & {AccountIds?: {[key: string]: string}}) {
    super(scope, id, props);

    const gitlabOidcProvider: OpenIdConnectProvider = new OpenIdConnectProvider(this, "GitlabProvider", {
      url: "https://gitlab.com",
      clientIds: ["https://gitlab.com"]
    });

    const repos: string [] = repositoriesConfig.map(
      repo => `project_path:${repo.group}/${repo.project}:ref_type:branch:ref:${repo.branch ?? "*"}`
    );
    
    const conditions: Conditions = {
      StringLike: {
        ["gitlab.com:sub"]: repos,
      }
    }

    const gitlabRole = new Role(this, "GitlabRole", {
      assumedBy: new WebIdentityPrincipal(
        gitlabOidcProvider.openIdConnectProviderArn,
        conditions
      ),
      roleName: "gitlab-oidc-role",
      description: "Gitlab OIDC Role for used in CI/CD Pipeline"
    });

    gitlabRole.attachInlinePolicy(
      new Policy(this, "GitlabPolciy", {
        document: new PolicyDocument({
          statements: [
            new PolicyStatement({
              actions: [
                "cloudformation:*",
                "s3:*",
              ],
              resources: ["*"]
            })
          ]
        })
      })
    );

    // gitlabRole.addManagedPolicy(
    //   ManagedPolicy.fromAwsManagedPolicyName("...")
    // )

  }
}
