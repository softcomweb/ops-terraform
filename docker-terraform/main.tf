terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "docker" {

}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.image_id
  name  = "softcomweb-nginx"
  ports {
    internal = 80
    external = 8900
  }

}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kind-kind-softcomweb-cluster"
}

resource "kubernetes_namespace" "softcomweb-test" {
  metadata {
    name = "softcomweb-test"
  }
}

resource "kubernetes_deployment" "softcomweb-test" {
  metadata {
    name      = "softcomweb-nginx"
    namespace = kubernetes_namespace.softcomweb-test.metadata.0.name
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "softcomweb-nginx"
      }
    }
    template {
      metadata {
        labels = {
          app = "softcomweb-nginx"
        }
      }
      spec {
        container {
          image = "nginx"
          name  = "softcomweb-nginx"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "softcomweb-test" {
  metadata {
    name      = "softcomweb-nginx"
    namespace = kubernetes_namespace.softcomweb-test.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.softcomweb-test.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      node_port   = 30201
      port        = 80
      target_port = 80
    }
  }
}
