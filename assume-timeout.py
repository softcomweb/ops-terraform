import boto3, botocore
import time
import os
from botocore.session import get_session


print("Hello World")

def get_credentials():
    print("Calling get_credentials function")
    session = boto3.session.Session()
    sts = session.client('sts')
    assume_cred = sts.assume_role(
        RoleArn=f'arn:aws:iam::250946034964:role/timeout-role',
        RoleSessionName='timeout_session',
    )["Credentials"]

    respone = {
        "access_key": assume_cred['AccessKeyId'],
        "secret_key": assume_cred['SecretAccessKey'],
        "token": assume_cred['SessionToken'],
        "expiry_time": assume_cred['Expiration'].isoformat()
    }
    print(f"Assume role response: {respone}")
    
    return respone



def get_refreshable_credentials(client: str, region: str):
    print("Getting refreshable credentials")
    credentials = botocore.credentials.RefreshableCredentials.create_from_metadata(
        metadata=get_credentials(),
        refresh_using=get_credentials,
        method='sts-assume-role',
    )

    session = get_session()
    session._credentials = credentials

    refresh_session = boto3.session.Session(botocore_session=session)

    client_session = refresh_session.client(client, region_name=region)

    print(f"Client Session: {client_session}")
    return client_session


def call_s3_with_sleep():
    # session for softcomweb-nonlive account
    print("Getting refreshable credentials")
    s3_session = get_refreshable_credentials(
        client="s3", 
        region="eu-central-1", 
    )
    
    print("Accessing s3 Bucket for Nonlive with account id: 359150914216")
    print(s3_session.list_buckets())
    time.sleep(3800)
    print("sleep for 3800 seconds")
    print(s3_session.list_buckets())

    # print("#################################")
    # # session for softcomweb-infra account
    # s3_session = get_refreshable_credentials(
    #     account_id="250946034964", 
    #     region="eu-central-1", 
    #     client="s3", 
    #     role_name="infrastructure-timeout-role"
    # )
    # print("Accessing s3 Bucket for Nonlive with account id: 250946034964")
    # s3_session = get_refreshable_credentials("s3", "eu-central-1")
    # print(s3_session.list_buckets())

print("Starting python run")
call_s3_with_sleep()
